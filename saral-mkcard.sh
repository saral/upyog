#!/bin/bash
# ------------------------------------------------------------------------------
# https://git.gitorious.org/saral/upyog.git
# ------------------------------------------------------------------------------
#
# saral-mkcard.sh
#
# A generic script to prepare SD card for embedded platforms.
#
#
# The MIT License
#
# Copyright (c) 2014 Sanjeev Premi (spremi at ymail.com).
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
# ------------------------------------------------------------------------------


# ------------------------------------------------------------------------------
# GLOBALS
# ------------------------------------------------------------------------------

#
# Name of script
#
SCRIPT_NAME=saral-mkcard.sh

#
# Version of script
#
SCRIPT_VERS=0.80

#
# Maximum size of an SD card.
# (Used as safety mechanism)
#
SZ_DISK_MAX=$(echo 64*1024*1024*1024 | bc)

#
# Prefixes for various messages
#
PLV0="\n ::: "
PLV1="\n  :: "
PLV2="   : "
PLV3="   : - "
PWRN=" [W] "
PERR="\n [E] "
PCNT="     "

#
# DOS compatibility: Bytes per sector
#
DOS_BYTES_P_SECTOR=512

#
# DOS compatibility: Sectors per track
#
DOS_SECTORS_P_TRACK=63

#
# DOS compatibility: Tracks per cylinder (Heads)
#
DOS_TRACKS_P_CYLINDER=255

# ------------------------------------------------------------------------------
# VARIABLES
# ------------------------------------------------------------------------------

#
# Argument: show help
#
arg_help=0

#
# Argument: Disk name
#
arg_disk=""

#
# Argument: Number of partitions
#
arg_npart=0

#
# Argument: Enable DOS compatibility
#
arg_dos=0

#
# Argument: Number of heads (TODO: Required?)
#
arg_heads=0

#
# Argument: Number of sectors (TODO: Required?)
#
arg_sectors=0

#
# Argument: Partition information
#
declare -a arg_parts=()

#
# Argument : Zero initial sectors
#
arg_zero=0

#
# Argument: Force partition as requested (despite warnings)
#
arg_force=0

#
# Argument : Show log
#
arg_log=0

#
# Status of execution
#
cmd_status=0

#
# Disk: Size in bytes
#
disk_size=0

#
# Disk: Number of cylinders
#
disk_cylinders=0

#
# Disk: Number of tracks per cylinder (Heads)
#
disk_trk_p_cyl=0

#
# Disk: Number of sectors per track
#
disk_sec_p_trk=0

#
# Disk: Number of bytes per sector
#
disk_byt_p_sec=0

#
# Name of log file
#
log_file="saral-mkcard.log"

# ------------------------------------------------------------------------------
# FUNCTIONS
# ------------------------------------------------------------------------------

#
# Print strings are various levels
#
function _p0
{
	echo -e "${PLV0}${@}"
}

function _p1
{
	echo -e "${PLV1}${@}"
}

function _p2
{
	echo -e "${PLV2}${@}"
}

function _p3
{
	echo -e "${PLV3}${@}"
}

function _pW
{
	echo -e "${PWRN}${@}"
}

function _pE
{
	echo -e "${PERR}${@}"
}

function _pC
{
	echo -e "${PCNT}${@}"
}

#
#
# Show banner
#
function banner
{
	_p0 "${SCRIPT_NAME}\tv${SCRIPT_VERS}"
}

#
# Show help
#
function show_help
{
	_p1 "Usage:"
	_p2 "    ${SCRIPT_NAME} -d <dev> -n parts -p pinfo1 -p pinfo2 [optargs]"
	_p2 ""
	_p2 "where,"
	_p2 "  -d dev"
	_p2 "\t\tTarget device to be partitioned"
	_p2 "  -n parts"
	_p2 "\t\tNumber of partitions to create"
	_p2 "  -p pinfo"
	_p2 "\t\tPartition information in the format:"
	_p2 "\t\t\tformat,size,label,boot"
	_p2 ""
	_p2 "\t\tformat: Desired format for the partition"
	_p2 "\t\tsize  : Number of sectors OR size in MB"
	_p2 "\t\t        (Size in MB ends with sufix M)"
	_p2 "\t\tlabel : Label to be used to identify partition"
	_p2 "\t\tboot  : If partition is bootable - 'b'"
	_p2 ""
	_p2 "\t\tThe partitions must be specified in desired sequence"
	_p2 ""
	_p2 "Optional arguments are:"
	_p2 "  -z num"
	_p2 "\t\tNumber of initial sectors to 'zero'"
	_p2 "  -h"
	_p2 "\t\tShow this help"
	_p2 "  -f"
	_p2 "\t\tFlag: Force override checks"
	_p2 "  -l"
	_p2 "\t\tFlag: Show detailed execution log before exiting"
	_p2 ""
	_p2 "  -D"
	_p2 "\t\tFlag: Enable DOS compatibility"
	_p2 ""
	_p2 "Additional parameters required(? TODO)"
	_p2 "  H   = number of heads"
	_p2 "  S   = Number of sectors"
	_p2 ""
	_p2 "Example (as root):"
	_p2 "  ./${SCRIPT_NAME} -d /dev/sde -n 2 -p fat16,64M,boot,b -p ext4,,rootfs -D -z 16"
	_p2 ""
	_p2 ""

	_p1 "\n"
}

#
# Add a step in the log file
#
function log_step
{
	echo -e "\n::::::::::::::: ${@}\n"	>> ${log_file}
}

#
# Execute command and append to log
#
function log_cmd
{
	${@}	>> ${log_file} 2>&1

	return ${?}
}

#
# Show command-line parameters
#
function show_params
{
	_p1 "Arguments:"

	_p2 "Device ................. ${arg_disk}"
	_p2 "Num Partitions ......... ${arg_npart}"
	_p2 "DOS compatibility ...... $([ ${arg_dos}   -eq 1 ] && echo "Y" || echo "N")"
	_p2 "Zero initial sectors ... $([ ${arg_zero}  -eq 0 ] && echo "N" || echo "Y")"
	_p2 "Show inline log ........ $([ ${arg_log}   -eq 1 ] && echo "Y" || echo "N")"
	_p2 "Force override ......... $([ ${arg_force} -eq 1 ] && echo "Y" || echo "N")"
}

#
# Parse command-line parameters
#
function parse_params
{
	while getopts "d:p:n:H:S:z:hfDl" opt; do

		case ${opt} in
		"h")
			arg_help=1
			;;

		"D")
			arg_dos=1
			disk_byt_p_sec=${DOS_BYTES_P_SECTOR}
			disk_sec_p_trk=${DOS_SECTORS_P_TRACK}
			disk_trk_p_cyl=${DOS_TRACKS_P_CYLINDER}
			;;

		"f")
			arg_force=1
			;;

		"l")
			arg_log=1
			;;

		"d")
			arg_disk=${OPTARG}
			;;

		"n")
			arg_npart=${OPTARG}
			;;

		"p")
			arg_parts+=(${OPTARG})
			;;

		"H")
			arg_heads=${OPTARG}
			;;

		"S")
			arg_sectors=${OPTARG}
			;;

		"z")
			arg_zero=${OPTARG}
			;;

		*)
			echo "Unknown argument: ${OPTARG}"
			;;
		esac
	done
}

#
# Verify command-line parameters
#
function verify_params
{
	_p1 "Verifying arguments"

	#
	# Device
	#
	_p2 "Device"

	# Any device specified?
	if [ "${arg_disk}" == "" ]; then
		_pE "No device specified."
		return 1
	fi

	# Does the specified device exist?
	if [ ! -b "${arg_disk}" ]; then
		_pE "Device '${arg_disk}' doesn't exist."

		return 1
	fi

	#
	# Partitions
	#
	_p2 "Partitions"

	# Check number of requested partitions
	if [ ${arg_npart} -eq 0 ]; then
		_pE "Number of partitions cannot be 0."
		_pC "Use option -n to specify the count."

		return 1
	fi

	if [ ${arg_npart} -gt 4 ]; then
		_pE "This script can create primary partitions only."
		_pC "Number of partitions cannot exceed 4."

		return 1
	fi

	if [ ${arg_npart} -ne ${#arg_parts[@]} ]; then
		_pE "Expected ${arg_npart} partitions"
		_pC "...got ${#arg_parts[@]} specifications."

		return 1
	fi

	return 0
}

#
# Check if user has permissions to run 'fdisk'
#
function check_perm_fdisk
{
	local check

	_p1 "Check permissions to use fdisk"

	check=$(fdisk -l ${arg_disk} 2>&1 | grep "Permission denied" | wc -l)

	if [ ${check} -ne 0 ]; then
		_pE "Can't run fdisk."
		_pC "You may need to run this script via sudo."

		return 1
	fi

	return 0
}

#
# Check if disk is present
#
function check_disk_present
{
	local check

	_p1 "Detect media presence"

	check=$(fdisk -l ${arg_disk} 2>&1 | grep "No medium found" | wc -l)

	if [ ${check} -ne 0 ]; then
		_pE "No medium found at ${arg_disk}"
		_pC "You may need to re-insert the SD card."

		return 1
	fi

	return 0
}

#
# Check if we aren't, possibly, formatting the host disk(s)
#
function is_host_disk
{
	_p1 "Is it host disk?"

	disk_size=$(fdisk -l ${arg_disk} | grep "Disk ${arg_disk}" | cut -f5 -d" ")

	if [ ${disk_size} -gt ${SZ_DISK_MAX} ]; then
		_pW "Size of '${arg_disk}' exceeds default threshold."

		if [ ${arg_force} -eq 1 ]; then
			_pC "Forced to continue..."
		else
			_pE "Is '${arg_disk}' right choice?"
			_pC "Use option -f to override this check."

			return 0
		fi
	fi

	_p2 "No. The disk size is ${disk_size} bytes"

	return 1
}

#
# Partition the disk
#
function partition_disk
{
	local ret

	_p1 "Partitioning the disk"

	#
	# Create command template for sfdisk
	#
	local _cmd_file=$(mktemp)

	echo "# partition table of ${arg_disk}" >> ${_cmd_file}
	echo "" >> ${_cmd_file}
	echo "unit: sectors" >> ${_cmd_file}
	echo "" >> ${_cmd_file}

	#
	# Initialize starting sector
	#
	local _start

	if [ ${arg_dos} -eq 1 ]; then
		_start=63
	else
		_pE "Not yet implemented"

		return 1
	fi

	for idx in $(seq 1 ${arg_npart})
	do
		local i
		local part_inf

		local _frmt		# Format
		local _sblk		# Start block
		local _nblk		# Num blocks
		local _size		# Size
		local _boot		# bootable?

		_p2 "Partition ${idx}"

		i=$(( idx - 1 ))

		part_inf=${arg_parts[${i}]}

		_frmt=$(echo ${part_inf} | cut -f1 -d",")
		_size=$(echo ${part_inf} | cut -f2 -d",")
		_boot=$(echo ${part_inf} | cut -f4 -d",")

		#
		# Derive partition id
		#
		local _id=""

		case ${_frmt} in
		"fat16")
			_id="6"
			;;

		"fat32")
			_id="c"
			;;

		"ext2"|"ext3"|"ext4")
			_id="83"
			;;

		*)
			_id="x"
			;;
		esac

		if [ ${_id} == "" ] || [ ${_id} == "x" ]; then
			_pE "Unknown format - '${_frmt}'"
			return 1
		fi

		#
		# Get partition size
		#
		local _len
		local _last
		local _align

		_len=${#_size}
		_last=${_size:$(( ${_len} - 1 )):1}

		if [ "${_last}" == "M" ]; then
			#
			# Derive blocks from the specified size
			#
			_size=${_size:0:$(( ${_len} - 1 ))}
			_nblk=$(( _size * 1024 * 1024 / disk_byt_p_sec ))

			#
			# Align blocks to cylinder boundary
			#
			_align=$(( (_start + _nblk) / disk_sec_p_trk / disk_trk_p_cyl ))

			_nblk=$(( _align * disk_sec_p_trk * disk_trk_p_cyl ))
			_nblk=$(( _nblk - _start ))
		else
			if [ "${_size}" == "" ]; then
				#
				# Auto-calculate size
				#
				_nblk=$(( (disk_size / ${disk_byt_p_sec}) - _start ))
				_size=$(( _nblk * disk_byt_p_sec / 1024 / 1024))

				#
				# Align blocks to cylinder boundary
				#
				_align=$(( (_start + _nblk) / disk_sec_p_trk / disk_trk_p_cyl ))

				_nblk=$(( _align * disk_sec_p_trk * disk_trk_p_cyl ))
				_nblk=$(( _nblk - _start ))
			else
				#
				# Get size (in MB) from specified blocks
				#
				_nblk=${_size}
				_size=$(( _nblk * disk_byt_p_sec / 1024 / 1024))
			fi
		fi

		#
		# Show partition information
		#
		_p3 "Start block .......... ${_start}"
		_p3 "Num blocks ........... ${_nblk} (${_size} MB)"
		_p3 "Id ................... ${_id}"

		#
		# Write partition information to command-file
		#
		local _part=$(printf "${arg_disk}${idx} : start=%9d, size=%9d, Id=%2s" ${_start} ${_nblk} ${_id})

		[ "${_boot}" == "b" ] && _part+=", bootable"

		echo -e "${_part}" >> ${_cmd_file}

		_start=$(( _start + _nblk ))
	done

	#
	# Complete information for remaining primary partitions
	#
	if [ ${arg_npart} -lt 4 ]; then
		local _num

		let _num=${arg_npart}+1

		for idx in $(seq ${_num} 4)
		do
			local _part=$(printf "${arg_disk}${idx} : start=%9d, size=%9d, Id=%2s" 0 0 0)

			echo -e "${_part}" >> ${_cmd_file}
		done
	fi

	log_step "Command file"

	log_cmd cat ${_cmd_file}

	#
	# Set arguments for compatibility mode
	#
	local _sfdisk_args=""

	if [ ${arg_dos} -eq 1 ]; then
		disk_cylinders=$(echo "${disk_size}/${disk_trk_p_cyl}/${disk_sec_p_trk}/${disk_byt_p_sec}" | bc)

		_sfdisk_args+=" -D -H ${disk_trk_p_cyl} -S ${disk_sec_p_trk} -C ${disk_cylinders}"
	else
		_pE "Not yet implemented"

		return 1
	fi

	#
	# Zero initial sectors
	#
	if [ ${arg_zero} -ne 0 ]; then
		_p2 "Zero initial sectors"

		log_step "Zero initial sectors"

		log_cmd dd if=/dev/zero of=${arg_disk} bs=${disk_byt_p_sec} count=${arg_zero}
		sync
	fi

	#
	# Apply the partitions
	#
	_p2 "Invoke sfdisk"

	log_step "Invoke sfdisk"

	log_cmd sfdisk ${_sfdisk_args} ${arg_disk} < ${_cmd_file}
	ret=${?}

	rm ${_cmd_file}

	return $ret
}

#
# Format partitions
#
function format_partitions
{
	local _part
	local _info
	local _format
	local _label
	local _i
	local _cmd

	_p1 "Format partitions"
	_pC "This step may take a while. Be patient...\n"

	for idx in $(seq 1 ${arg_npart})
	do
		let i=${idx}-1

		_part=${arg_disk}${idx}
		_info=${arg_parts[${i}]}

		log_step "Format ${_part}"

		_format=$(echo ${_info} | cut -f1 -d",")
		_label=$(echo ${_info} | cut -f3 -d",")

		[ "${_label}" == "" ] && _label="part${idx}"

		#
		# Show format details
		#
		_p2 "${_part}"
		_p3 "Format ............... ${_format}"
		_p3 "Label ................ ${_label}"

		case ${_format} in
		"fat16")
			_cmd="mkfs.fat -F 16 -n ${_label} ${_part}"
			;;

		"fat32")
			_cmd="mkfs.fat -F 32 -n ${_label} ${_part}"
			;;

		"ext2")
			_cmd="mkfs.ext2 -L ${_label} ${_part}"
			;;

		"ext3")
			_cmd="mkfs.ext3 -j -L ${_label} ${_part}"
			;;

		"ext4")
			_cmd="mkfs.ext4 -j -L ${_label} ${_part}"
			;;

		*)
			_cmd=""
			_pE "Unknown format - ${_format}"
			;;
		esac

		[ "${_cmd}" == "" ] && return 1

		log_cmd ${_cmd}
		[ ${?} -ne 0 ] && return 1
	done

	return 0
}

# ------------------------------------------------------------------------------
# BEGIN
# ------------------------------------------------------------------------------

#
# Show banner
#
banner

#
# Get command-line parameters
#
if [ $# -gt 0 ]; then
	parse_params "${@}"
else
	arg_help=1
fi

#
# Show help & exit - if required.
#
[ ${arg_help} -eq 1 ] && show_help && exit 0

show_params

#
# Verify parameters
#
verify_params
cmd_status=${?}

#
# Prime the log file
#
[ -f ${log_file} ] && rm -f ${log_file}

touch ${log_file}

log_step "$(date)"

#
# Check if we can proceed with 'fdisk'
#
if [ ${cmd_status} -eq 0 ]; then

	check_perm_fdisk
	cmd_status=${?}

	if [ ${cmd_status} -eq 0 ]; then
		check_disk_present
		cmd_status=${?}
	fi
fi

#
# Check if we should proceed with selected disk
#
if [ ${cmd_status} -eq 0 ]; then
	is_host_disk
	if [ ${?} -eq 0 ]; then
		cmd_status=1
	fi
fi

#
# Proceed with partitioning
#
if [ ${cmd_status} -eq 0 ]; then
	partition_disk
	cmd_status=${?}

	if [ ${cmd_status} -eq 0 ]; then
		#
		# Inform kernel on changes to the partition table
		#
		_p1 "Reading updated partition table"

		log_step partprobe

		log_cmd partprobe -s ${arg_disk}

		#
		# Remove partitions from device mapper.
		# Else, they are reported as 'busy'
		#
		_p1 "Remove partitions from device mapper"

		for idx in $(seq 1 ${arg_npart})
		do
			_part=/dev/mapper/$(basename ${arg_disk}${idx})

			_p2 "${_part}"

			log_step "Remove ${_part} from device mapper"

			[ -b ${_part} ] && log_cmd dmsetup remove ${_part}
		done

		#
		# Format partitions
		#
		format_partitions
		cmd_status=${?}
	fi
fi


# ------------------------------------------------------------------------------
# END
# ------------------------------------------------------------------------------
if [ ${cmd_status} -eq 0 ]; then
	_p0 "Success!\n"
else
	_p0 "Failed :( Try again...\n"
fi

if [ ${arg_log} -eq 1 ]; then
	_p2 " ---- Execution details ----------\n"
	cat ${log_file}
	_p2 " ---------------------------------\n"
else
	_p2 "See execution details in '${log_file}'.\n"
fi

exit ${cmd_status}
